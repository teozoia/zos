.section ".text.boot"

.globl _start

_start:

	adr	x20, stack + (1 << 15)
	mov	sp, x20

	/* Clear out bss */
	ldr	x4, =__bss_start
	ldr	x9, =__bss_end
	mov	x5, #0
	mov	x6, #0

	b	.test

	/* this does 2x8 = 16 byte stores at once */
.loop:
	stp	x5, x6, [x4, #0]
	add	x4, x4, #16
.test:
	cmp	x4, x9
	blo	.loop
	
	/* Single core mode */
single_core:
	mrs x6, MPIDR_EL1						// Fetch core Id
	and x6, x6, #0x3						// Create 2 bit mask of core Id
    cmp x6, #0									// Check core id=0 (first core)
    bne halt

	/* branch and link to kernel_main */
	bl	kernel_main

    /* 0x20026 == ADP_Stopped_ApplicationExit */
    mov x1, #0x26
    movk x1, #2, lsl #16
    str x1, [sp,#0]

    /* Exit status code. Host QEMU process exits with that status. */
    mov x0, #0
    str x0, [sp,#8]

    /* x1 contains the address of parameter block.
     * Any memory address could be used. */
    mov x1, sp

    /* SYS_EXIT */
    mov w0, #0x18

    /* Do the semihosting call on A64. */
    hlt 0xf000

	/* Halt cores and also halt kernel_main return */
halt:
	wfe													// equivalent of x86 HLT instruction
	b	halt


.section ".bss"
	.align 4		/* Align on 128bit boundary */
	stack:
	.align 15		/* Reserve 32kb of stack */

