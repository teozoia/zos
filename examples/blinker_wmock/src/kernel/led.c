#include <led.h>
#include <gpio.h>

#include <stdbool.h>

void led_init(led_t *led, uint8_t pin){

    led->pin = pin;
    led->status = 0;
    
    gpio_setup(led->pin, GPIO_OUTPUT);
    gpio_output(led->pin, false);
}

uint8_t led_toggle(led_t *led){
    if(led->status == 0){
        led->status = 1;
        gpio_output(led->pin, true);
    }else{
        led->status = 0;
        gpio_output(led->pin, false);
    }
		return led->status;
}
