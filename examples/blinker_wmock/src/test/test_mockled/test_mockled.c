#include <unity.h>
#include <Mockled.h>

void setUp(void){}

void tearDown(void){}

void init_Callback(led_t* led, uint8_t pin, int cmock_num_calls){}

uint8_t broken_led_Callback(uint32_t *led, int cmock_num_calls){ return 0; }

uint8_t working_led_Callback(uint32_t *led, int cmock_num_calls){
    if(cmock_num_calls % 2 == 0)
        return 0;
    return 1;
}

void test_without_callback(void){
    
    led_t led;
    led_init(&led, 22);
    
    led_toggle_ExpectAndReturn(&led, 0); // broken led
    int v1 = led_toggle(&led);
    int v2 = led_toggle(&led);
    TEST_ASSERT_FALSE( v1 == v2 );
}

void test_broken_led(void) {
    
    // This tells CMock to use our callback.
    led_init_StubWithCallback(init_Callback);
    led_toggle_StubWithCallback(broken_led_Callback);
    // You still need ExpectAndReturn's
    // The return value from the _ExpectAndReturn is ignored
    // the return value from the callback is used instead.
	
	led_t led;
	led_init(&led, 21);
    
    TEST_ASSERT_FALSE( led_toggle(&led) == led_toggle(&led) );
}

void test_working_led(void) {
	
    led_init_StubWithCallback(init_Callback);
    led_toggle_StubWithCallback(working_led_Callback);
    
    led_t led;
    led_init(&led, 23);
    
    TEST_ASSERT_EQUAL_HEX8(0x0, led_toggle(&led));
    TEST_ASSERT_EQUAL_INT(1, led_toggle(&led));
}
