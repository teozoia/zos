CC = ./../../../../gcc-arm-8.3-2019.03-x86_64-aarch64-elf/bin/aarch64-elf-gcc
ARMGNU = ./../../../../gcc-arm-8.3-2019.03-x86_64-aarch64-elf/bin/aarch64-elf
CPU = cortex-a53+fp+simd
CFLAGS = -mcpu=$(CPU) -fpic -ffreestanding $(DIRECTIVES)
CSRCFLAGS = -O3 -Wall -Wextra
LFLAGS = -ffreestanding -O3 -nostdlib -nostartfiles -std=c11 -mstrict-align -fno-tree-loop-vectorize -fno-tree-slp-vectorize -Wno-nonnull-compare
LINKER = linker.ld
TEST_LINKER = test_linker.ld
LIST = kernel.list
MAP = kernel.map

# Location of files
KER_SRC = ../src/kernel
KER_HEAD = ../include/kernel
TEST_SRC = ../src/test/$(TEST)
TEST_HEAD = ../include/test/$(TEST)
UNITY_SRC = ../src/test
UNITY_HEAD = ../include/test
TEST_OBJ_DIR = $(TEST)/objects
MOCK_SRC = ../mocks
MOCK_HEAD = ../mocks

# test build
KERSOURCES = $(wildcard $(KER_SRC)/*.c)
TESTASMSOURCES = $(wildcard $(UNITY_SRC)/*.S)
UNITYSOURCES = $(wildcard $(UNITY_SRC)/*.c)
TESTSOURCES = $(wildcard $(TEST_SRC)/*.c)
MOCKSOURCES = $(wildcard $(MOCK_SRC)/*.c)

TEST_OBJECTS += $(patsubst $(KER_SRC)/%.c, $(TEST_OBJ_DIR)/%.o, $(KERSOURCES))
TEST_OBJECTS += $(patsubst $(UNITY_SRC)/%.S, $(TEST_OBJ_DIR)/%.o, $(TESTASMSOURCES))
TEST_OBJECTS += $(patsubst $(UNITY_SRC)/%.c, $(TEST_OBJ_DIR)/%.o, $(UNITYSOURCES))
TEST_OBJECTS += $(patsubst $(TEST_SRC)/%.c, $(TEST_OBJ_DIR)/%.o, $(TESTSOURCES))
TEST_OBJECTS += $(patsubst $(MOCK_SRC)/%.c, $(TEST_OBJ_DIR)/%.o, $(MOCKSOURCES))

TEST_HEADERS = $(wildcard $(KER_HEAD)/*.h)
TEST_HEADERS += $(wildcard $(UNITY_HEAD)/*.h)
TEST_HEADERS += $(wildcard $(TEST_HEAD)/*.h)
TEST_HEADERS += $(wildcard $(MOCK_HEAD)/*.h)

# build
OBJ_DIR = objects
ASMSOURCES = $(wildcard $(KER_SRC)/*.S)
OBJECTS = $(patsubst $(KER_SRC)/%.c, $(OBJ_DIR)/%.o, $(KERSOURCES))
OBJECTS += $(patsubst $(KER_SRC)/%.S, $(OBJ_DIR)/%.o, $(ASMSOURCES))
HEADERS = $(wildcard $(KER_HEAD)/*.h)

IMG_NAME=zos.img
TEST_IMG_NAME=$(TEST).img
IMG_DEPLOY=../deploy/kernel8.img

mock:
	cd ..; ruby lib/cmock.rb $(MOCK)
	@echo "Mock created!"

# build
build: $(OBJECTS) $(HEADERS)
	$(ARMGNU)-gcc $(LFLAGS) -T $(LINKER) -Wl,--build-id=none -o kernel.elf -lc -lm -lgcc $(OBJECTS)
	$(ARMGNU)-objdump -d kernel.elf > $(LIST)
	$(ARMGNU)-objcopy kernel.elf -O binary $(IMG_NAME)
	$(ARMGNU)-nm -n kernel.elf > $(MAP)
	cp $(IMG_NAME) $(IMG_DEPLOY)

$(OBJ_DIR)/%.o: $(KER_SRC)/%.c
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -I$(KER_SRC) -I$(KER_HEAD) -c $< -o $@ $(CSRCFLAGS)

$(OBJ_DIR)/%.o: $(KER_SRC)/%.S
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -I$(KER_SRC) -c $< -o $@

clean:
	rm -rf $(OBJ_DIR)
	rm -f $(IMG_NAME)
	rm -f kernel.elf
	rm -f kernel.list
	rm -f kernel.map
	rm -f $(IMG_DEPLOY)
	
run: build
	qemu-system-aarch64 -m 1024 -M raspi3 -serial stdio -kernel $(IMG_NAME)

gdb: build
	qemu-system-aarch64 -S -gdb tcp::9000 -m 1024 -M raspi3 -serial stdio -kernel $(IMG_NAME)

# test build
testbuild: testcheck $(TEST_OBJECTS) $(TEST_HEADERS)
	mkdir -p $(TEST)
	echo "testrun:\n\tqemu-system-aarch64 -m 1024 -M raspi3 -serial stdio -semihosting -kernel \"$(TEST_IMG_NAME)\"" > $(TEST)/Makefile
	ruby ../auto/generate_test_runner.rb ../src/test/$(TEST)/$(TEST).c ../src/test/$(TEST)/$(TEST)_Runner.c
	$(ARMGNU)-gcc $(LFLAGS) -T $(LINKER) -Wl,--build-id=none -o $(TEST)/$(TEST).elf -lc -lm -lgcc $(filter-out $(TEST)/objects/$(MOCKED).o, $(TEST_OBJECTS))
	$(ARMGNU)-objcopy $(TEST)/$(TEST).elf -O binary $(TEST)/$(TEST_IMG_NAME)

$(TEST_OBJ_DIR)/%.o: $(KER_SRC)/%.c
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -I$(KER_SRC) -I$(KER_HEAD) -c $< -o $@ $(CSRCFLAGS)
	
$(TEST_OBJ_DIR)/%.o: $(UNITY_SRC)/%.c
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -I$(KER_SRC) -I$(KER_HEAD) -I$(TEST_SRC) -I$(TEST_HEAD) -I$(UNITY_SRC) -I$(UNITY_HEAD) -I$(MOCK_SRC) -I$(MOCK_HEAD) -c $< -o $@ $(CSRCFLAGS)

$(TEST_OBJ_DIR)/%.o: $(MOCK_SRC)/%.c
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -I$(KER_SRC) -I$(KER_HEAD) -I$(TEST_SRC) -I$(TEST_HEAD) -I$(UNITY_SRC) -I$(UNITY_HEAD) -I$(MOCK_SRC) -I$(MOCK_HEAD) -c $< -o $@ $(CSRCFLAGS)
	
$(TEST_OBJ_DIR)/%.o: $(TEST_SRC)/%.c
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -I$(KER_SRC) -I$(KER_HEAD) -I$(TEST_SRC) -I$(TEST_HEAD) -I$(UNITY_SRC) -I$(UNITY_HEAD) -I$(MOCK_SRC) -I$(MOCK_HEAD) -c $< -o $@ $(CSRCFLAGS)

$(TEST_OBJ_DIR)/%.o: $(UNITY_SRC)/%.S
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -I$(UNITY_SRC) -c $< -o $@


testclean: testcheck
	rm -f $(TEST)/$(TEST_IMG_NAME)
	rm -rf $(TEST_OBJ_DIR)
	rm -rf $(TEST)
	
testrun: testbuild
	qemu-system-aarch64 -m 1024 -M raspi3 -serial stdio -semihosting -kernel $(TEST)/$(TEST_IMG_NAME)
	
mockclean:
	rm -rf ../mocks
	
testcheck:
ifndef TEST
	$(error ERROR: TEST is undefined, provide: TEST=test_something)
endif
