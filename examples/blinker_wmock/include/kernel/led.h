#ifndef _LED_
#define _LED_

#include <stdbool.h>      
#include <stdint.h>

typedef struct led {
    uint8_t pin: 7;             // Rpi 3b+ pin
    uint8_t status: 1;          // on/off
} led_t;

void led_init(led_t *led, uint8_t pin);
uint8_t led_toggle(led_t *led);

#endif // LED_H
