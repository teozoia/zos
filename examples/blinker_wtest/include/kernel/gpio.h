#ifndef _GPIO_
#define _GPIO_

#include <stdbool.h>					// Needed for bool and true/false
#include <stdint.h>						// Needed for uint8_t, uint32_t, etc

#define MMIO_BASE       0x3F000000

#define GPIO_BASE        (MMIO_BASE + 0x00200000)

#define GPFSEL0         (GPIO_BASE + 0x00)
#define GPFSEL1         (GPIO_BASE + 0x04)
#define GPFSEL2         (GPIO_BASE + 0x08)
#define GPFSEL3         (GPIO_BASE + 0x0C)
#define GPFSEL4         (GPIO_BASE + 0x10)
#define GPFSEL5         (GPIO_BASE + 0x14)
#define GPSET0          (GPIO_BASE + 0x1C)
#define GPSET1          (GPIO_BASE + 0x20)
#define GPCLR0          (GPIO_BASE + 0x28)
#define GPLEV0          (GPIO_BASE + 0x34)
#define GPLEV1          (GPIO_BASE + 0x38)
#define GPEDS0          (GPIO_BASE + 0x40)
#define GPEDS1          (GPIO_BASE + 0x44)
#define GPHEN0          (GPIO_BASE + 0x64)
#define GPHEN1          (GPIO_BASE + 0x68)
#define GPPUD           (GPIO_BASE + 0x94)
#define GPPUDCLK0       (GPIO_BASE + 0x98)
#define GPPUDCLK1       (GPIO_BASE + 0x9C)

typedef enum {
	GPIO_INPUT 	  = 0b000,				// 0
	GPIO_OUTPUT   =	0b001,				// 1
	GPIO_ALTFUNC5 = 0b010,				// 2
	GPIO_ALTFUNC4 = 0b011,				// 3
	GPIO_ALTFUNC0 = 0b100,				// 4
	GPIO_ALTFUNC1 = 0b101,				// 5
	GPIO_ALTFUNC2 = 0b110,				// 6
	GPIO_ALTFUNC3 = 0b111,				// 7
} GPIOMODE;

void mmio_write(uint32_t reg, uint32_t data);
uint32_t mmio_read(uint32_t reg);

bool gpio_setup (uint8_t gpio, GPIOMODE mode);
bool gpio_output (uint8_t gpio, bool on);
bool gpio_input (uint8_t gpio);

#endif
