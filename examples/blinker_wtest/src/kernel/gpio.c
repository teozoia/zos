#include <stdbool.h>		// Needed for bool and true/false
#include <stdint.h>			// Needed for uint8_t, uint32_t, uint64_t etc
#include <gpio.h>			// This units header

/* Memory-Mapped I/O output x0 addr, w1 */
void mmio_write(uint32_t reg, uint32_t data){
    __asm__("str w1, [x0]");
}
 
/* Memory-Mapped I/O input */
uint32_t mmio_read(uint32_t reg){
    __asm__("ldr x0, [x0]");
}

bool gpio_setup (uint8_t gpio, GPIOMODE mode) {
    
    if (gpio > 54)
        return false;                                        // Check GPIO pin number valid, return false if invalid
    if (mode < 0 || mode > GPIO_ALTFUNC3)
        return false;                                        // Check requested mode is valid, return false if invalid
    
    uint32_t bit = ((gpio % 10) * 3);                        // Create bit mask
    uint32_t mem = mmio_read(GPFSEL0 + (gpio / 10 * 4));    // Read register
    
    mem &= ~(7 << bit);                                        // Clear GPIO mode bits for that port
    mem |= (mode << bit);                                    // Logical OR GPIO mode bits
    mmio_write(GPFSEL0 + (gpio / 10 * 4), mem);
    
    return true;                                            // Return true
}

bool gpio_output (uint8_t gpio, bool on) {
    
    if (gpio < 54){                                         // Check GPIO pin number valid, return false if invalid
        
        uint32_t regnum = gpio / 32;                        // Register number
        uint32_t bit = 1 << (gpio % 32);                    // Create mask bit
        
        if(on)
            mmio_write(GPSET0 + regnum, bit);                // On == true means set
        else
            mmio_write(GPCLR0 + regnum, bit);                // On == false means clear
        
        return true;                                        // Return true
    }
    
    return false;                                            // Return false
}

bool gpio_input (uint8_t gpio) {
    
    if (gpio < 54){                                            // Check GPIO pin number valid, return false if invalid
        uint32_t bit = 1 << (gpio % 32);                    // Create mask bit
        uint32_t mem = mmio_read(GPLEV0 + (gpio / 32));        // Read port level
        
        if (mem & bit)
            return true;                                    // Return true if bit set
    }
    
    return false;                                            // Return false
}
