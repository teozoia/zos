#include <stdbool.h>
#include <stdint.h>
#include <gpio.h>
#include <led.h>

void kernel_main (void) {
    
	led_t led;
	led_init(&led, 21);
	
	while(1) {
    led_toggle(&led);
		
		for(int i = 0; i < 0x3f0000; i++){
			__asm__("nop");
		}
	}
    
}
