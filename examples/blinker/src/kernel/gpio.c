#include <stdbool.h>														// Needed for bool and true/false
#include <stdint.h>															// Needed for uint8_t, uint32_t, uint64_t etc
#include <gpio.h>																// This units header

bool gpio_setup (uint8_t gpio, GPIOMODE mode) {
	
	if (gpio > 54)
		return false;																// Check GPIO pin number valid, return false if invalid
	if (mode < 0 || mode > GPIO_ALTFUNC3)
		return false;																// Check requested mode is valid, return false if invalid
	
	uint32_t bit = ((gpio % 10) * 3);							// Create bit mask
	uint32_t mem = GPIO->GPFSEL[gpio / 10];				// Read register
	
	mem &= ~(7 << bit);														// Clear GPIO mode bits for that port
	mem |= (mode << bit);													// Logical OR GPIO mode bits
	GPIO->GPFSEL[gpio / 10] = mem;								// Write value to register
	
	return true;																	// Return true
}

bool gpio_output (uint8_t gpio, bool on) {
	
	if (gpio < 54){ 															// Check GPIO pin number valid, return false if invalid
		
		uint32_t regnum = gpio / 32;								// Register number
		uint32_t bit = 1 << (gpio % 32);						// Create mask bit
		volatile uint32_t* p;												// Create temp pointer
		
		if(on)
			p = &GPIO->GPSET[regnum];									// On == true means set
		else
			p = &GPIO->GPCLR[regnum];									// On == false means clear
		*p = bit;																		// Output bit	
		
		return true;																// Return true
	}
	
	return false;																	// Return false
}

bool gpio_input (uint8_t gpio) {
	
	if (gpio < 54){																// Check GPIO pin number valid, return false if invalid
		uint32_t bit = 1 << (gpio % 32);						// Create mask bit
		uint32_t mem = GPIO->GPLEV[gpio / 32];			// Read port level
		
		if (mem & bit)
			return true;															// Return true if bit set
	}
	
	return false;																	// Return false
}

// Loop <delay> times in a way that the compiler won't optimize away
void delay(int32_t count) {
    for(int32_t i = 0; i < count; i++)
			__asm__("nop");
	
}

