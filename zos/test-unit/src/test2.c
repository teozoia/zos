#include "unity.h"
#include "gpio.h"

void test_function_gpio_setup1(void) {
    #ifdef IGNORE_GPIO_SETUP1
    	TEST_IGNORE();
	#else
    	TEST_ASSERT( gpio_setup(20, GPIO_OUTPUT) == true );
	#endif
    
}

void test_function_gpio_setup2(void) {
    //test stuff
	TEST_ASSERT( gpio_setup(20, GPIO_OUTPUT) == false );
}
