#include "unity.h"
#include "gpio.h"

#define IGNORE_GPIO_INPUT1

void test_function_gpio_input1(void) {
    #ifdef IGNORE_GPIO_INPUT1
    	TEST_IGNORE();
	#else
    	TEST_ASSERT( gpio_input(23) == true );
	#endif
    
}

void test_function_gpio_input2(void) {
    //test stuff
	TEST_ASSERT( gpio_input(76) == false );
}