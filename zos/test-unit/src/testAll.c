/*
 * WRAPPER TO ENCLOSE ALL TESTS
 */
#include "unity.h"

extern void test_function_gpio_input1(void);
extern void test_function_gpio_input2(void);
extern void test_function_gpio_setup1(void);
extern void test_function_gpio_setup2(void);

void setUp(){
	TEST_MESSAGE("Setup");
}

void tearDown(){}

int testAll(void) {

    UNITY_BEGIN();

    RUN_TEST(test_function_gpio_input1);
    RUN_TEST(test_function_gpio_input2);
    RUN_TEST(test_function_gpio_setup1);
    RUN_TEST(test_function_gpio_setup2);

    return UNITY_END();
}