#include <atag.h>
#include <uart.h>

uint32_t get_page_size(atag_t *tag) {
    
    while (tag->hdr.tag != ATAG_NONE){
        if(tag->hdr.tag == ATAG_CORE)
            return tag->u.core.pagesize;
        tag = (atag_t *)((uint32_t *)tag + tag->hdr.size);
    }

    return 0;
}

uint32_t get_mem_start(atag_t *tag) {
    
    while (tag->hdr.tag != ATAG_NONE){
        if(tag->hdr.tag == ATAG_MEM)
            return tag->u.mem.start;
        tag = (atag_t *)((uint32_t *)tag + tag->hdr.size);
    }

    return 0;
}

uint32_t get_mem_size(atag_t *tag) {
    
    while (tag->hdr.tag != ATAG_NONE){
        if(tag->hdr.tag == ATAG_MEM)
            return tag->u.mem.size;
        tag = (atag_t *)((uint32_t *)tag + tag->hdr.size);
    }

    return 0;
}

void dump_atags(atag_t *ptr){

    atag_t *tags = ptr;
    uart_printf("ATAGS at 0x%x\n", tags);
    
    while (tags->hdr.tag != ATAG_NONE)
    {
        switch (tags->hdr.tag)
        {
            case ATAG_CORE:
                uart_printf("ATAG_CORE\n");
                uart_printf("  flags = 0x%x\n", tags->u.core.flags);
                uart_printf("  pagesize = 0x%x\n", tags->u.core.pagesize);
                uart_printf("  rootdev = 0x%x\n", tags->u.core.rootdev);
                break;
            
            case ATAG_MEM:
                uart_printf("ATAG_MEM\n");
                uart_printf("  size = 0x%x\n", tags->u.mem.size);
                uart_printf("  start = 0x%x\n", tags->u.mem.start);
                break;
            
            case ATAG_VIDEOTEXT:
                uart_printf("ATAG_VIDEOTEXT\r\n");
                break;
            
            case ATAG_RAMDISK:
                uart_printf("ATAG_RAMDISK\r\n");
                break;
            
            case ATAG_INITRD2:
                uart_printf("ATAG_INITRD2\r\n");
                break;
            
            case ATAG_SERIAL:
                uart_printf("ATAG_SERIAL\r\n");
                break;
            
            case ATAG_REVISION:
                uart_printf("ATAG_REVISION\r\n");
                break;
            
            case ATAG_VIDEOLFB:
                uart_printf("ATAG_VIDEOLFB\r\n");
                break;
            
            case ATAG_CMDLINE:
                uart_printf("ATAG_CMDLINE\r\n");
                uart_printf("  %s\r\n", &tags->u.cmdline.cmdline);
                break;
            
            default:
                uart_printf("unknown\r\n");
                break;
        }
        
        tags = (atag_t *)((uint32_t *)tags + tags->hdr.size);
    }
}