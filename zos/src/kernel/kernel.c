#include <stdbool.h>							// C standard needed for bool
#include <stdint.h>								// C standard for uint8_t, uint16_t, uint32_t etc
#include <gpio.h>
#include <uart.h>
#include <atag.h>
#include <mem.h>

/*
void print_cpsr(){ 

	__asm__("mrs x0, CurrentEL;" : : : "%x0");
	__asm__("mov x0, x0, lsr 2");
	__asm__("bl itoa");
	__asm__("bl uart_puts");
}
*/

void kernel_main (uint32_t atags) {
	
	gpio_setup(20,GPIO_OUTPUT);
	gpio_output(20, true);

	uart_init();
	uart_printf("Hello, kernel World!\n");
	
	//print_cpsr();
	dump_atags((void *)atags);

	mem_init((atag_t *)atags);

	int t = 0xf000;
	while(1){
		gpio_output(20, true);					// Set GPIO 20 in UP state
		delay(t);
		
		gpio_output(20, false);					// Set GPIO 20 to DOWN state
		delay(t);

		t -= 0x100;
		if(t < 0x200)
			t = 0xf000;
	}

	uart_printf("Bye!\n");

}