#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include <printf.h>

#include <gpio.h>
#include <uart.h>

void uart_init(){

    mmio_write(UART0_CR, 0x00000000);

    mmio_write(GPPUD, 0x00000000);
    delay(150);

    mmio_write(GPPUDCLK0, (1 << 14) | (1 << 15));
    delay(150);

    mmio_write(GPPUDCLK0, 0x00000000);

    mmio_write(UART0_ICR, 0x7FF);

    mmio_write(UART0_IBRD, 1);
    mmio_write(UART0_FBRD, 40);

    mmio_write(UART0_LCRH, (1 << 4) | (1 << 5) | (1 << 6));

    mmio_write(UART0_IMSC, (1 << 1) | (1 << 4) | (1 << 5) | (1 << 6) |
        (1 << 7) | (1 << 8) | (1 << 9) | (1 << 10));

    mmio_write(UART0_CR, (1 << 0) | (1 << 8) | (1 << 9));
}

void uart_putc(unsigned char c){

    while ( mmio_read(UART0_FR) & (1 << 5) ) { /* do-nothing */ }
    mmio_write(UART0_DR, c);
}

unsigned char uart_getc(){

    while ( mmio_read(UART0_FR) & (1 << 4) ) { /* do-nothing */ }
    return mmio_read(UART0_DR);
}

void uart_puts(const char* str){

    for (size_t i = 0; str[i] != '\0'; i ++)
        uart_putc((unsigned char)str[i]);
}

void uart_printf(const char *format, ...) {
    va_list args;
    va_start(args, format);

    char buffer[512];
    vsnprintf(buffer, sizeof(buffer), format, args);

    char *s = buffer;
    for (size_t i = 0; i < sizeof(buffer) && *s != 0; i++, s++) {
        uart_putc(*s);
    }
}
