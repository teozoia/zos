#include <stdint.h>
#include <stddef.h>
#include <atag.h>
#include <string.h>
#include <mem.h>
#include <gpio.h>

extern uint8_t __end;
static uint32_t num_pages;

static page_t *all_pages_array;
page_list_t free_pages;

static heap_segment_t *heap_segment_list_head;

/**
 * impliment kmalloc as a linked list of allocated segments.
 * Segments should be 4 byte aligned.
 * Use best fit algorithm to find an allocation
 */

void mem_init(atag_t *atags) {

    // Get the total number of pages
    uint32_t mem_size = get_mem_size(atags);
    num_pages = mem_size / PAGE_SIZE;

    // Allocate space for all those pages metadata.
    // Start this block just after the kernel image is finished
    uint32_t page_array_len = sizeof(page_t) * num_pages;
    all_pages_array = (page_t *)&__end;

    bzero(all_pages_array, page_array_len);
    page_list_init(&free_pages);

    // Iterate over all pages and mark them with the appropriate flags
    // Start with kernel pages
    uint32_t kernel_pages = ((uint32_t)&__end) / PAGE_SIZE;
    uint32_t i = 0;
    for (i = 0; i < kernel_pages; i++) {
        all_pages_array[i].vaddr = i * PAGE_SIZE;    // Identity map the kernel pages
        all_pages_array[i].flags.allocated = 1;
        all_pages_array[i].flags.kernel_page = 1;
    }

    // Reserve 1 MB for the kernel heap
    for(; i < kernel_pages + (KERNEL_HEAP_SIZE / PAGE_SIZE); i++){
        all_pages_array[i].vaddr = i * PAGE_SIZE;    // Identity map the kernel pages
        all_pages_array[i].flags.allocated = 1;
        all_pages_array[i].flags.kernel_heap_page = 1;
    }

    // Map the rest of the pages as unallocated, and add them to the free list
    for(; i < num_pages; i++){
        all_pages_array[i].flags.allocated = 0;
        page_list_append(&free_pages, &all_pages_array[i]);
    }

    // Initialize the heap
    uint32_t page_array_end = (uint32_t)&__end + page_array_len;
    heap_init(page_array_end); 

}

void *alloc_page(void){

    if (page_list_size(&free_pages) == 0)
        return 0;

    // Get a free page
    page_t *page = page_list_pop(&free_pages);
    page->flags.kernel_page = 1;
    page->flags.allocated = 1;

    // Get the address the physical page metadata refers to
    void *page_mem = (void *)((page - all_pages_array) * PAGE_SIZE);

    // Zero out the page, big security flaw to not do this :)
    bzero(page_mem, PAGE_SIZE);

    return page_mem;
}

void free_page(void *ptr){

    // Get page metadata from the physical address
    page_t *page = all_pages_array + ((uint32_t)ptr / PAGE_SIZE); 

    page->flags.allocated = 0;              // Mark the page as free
    page_list_append(&free_pages, page);
}

/* Heap */

void heap_init(uint32_t heap_start) {
   heap_segment_list_head = (heap_segment_t *) heap_start;
   bzero(heap_segment_list_head, sizeof(heap_segment_t));
   heap_segment_list_head->segment_size = KERNEL_HEAP_SIZE;
}


void *kmalloc(uint32_t bytes) {
    heap_segment_t * curr, *best = NULL;
    int diff, best_diff = 0x7FFFFFFF; // Max signed int

    // Add the header to the number of bytes we need and make the size 4 byte aligned
    bytes += sizeof(heap_segment_t);
    bytes += bytes % 16 ? 16 - (bytes % 16) : 0;

    // Find the allocation that is closest in size to this request
    for (curr = heap_segment_list_head; curr != NULL; curr = curr->next) {
        diff = curr->segment_size - bytes;
        if (!curr->is_allocated && diff < best_diff && diff >= 0) {
            best = curr;
            best_diff = diff;
        }
    }

    // There must be no free memory right now :(
    if (best == NULL)
        return NULL;

    // If the best difference we could come up with was large, split up this segment into two.
    // Since our segment headers are rather large, the criterion for splitting the segment is that
    // when split, the segment not being requested should be twice a header size
    if (best_diff > (int)(2 * sizeof(heap_segment_t))) {
        bzero(((void*)(best)) + bytes, sizeof(heap_segment_t));
        curr = best->next;
        best->next = ((void*)(best)) + bytes;
        best->next->next = curr;
        best->next->prev = best;
        best->next->segment_size = best->segment_size - bytes;
        best->segment_size = bytes;
    }

    best->is_allocated = 1;

    return best + 1;
}

void kfree(void *ptr) {
    heap_segment_t * seg;

    if (!ptr)
        return;

    seg = ptr - sizeof(heap_segment_t);
    seg->is_allocated = 0;

    // try to coalesce segements to the left
    while(seg->prev != NULL && !seg->prev->is_allocated) {
        seg->prev->next = seg->next;
        seg->next->prev = seg->prev;
        seg->prev->segment_size += seg->segment_size;
        seg = seg->prev;
    }
    // try to coalesce segments to the right
    while(seg->next != NULL && !seg->next->is_allocated) {
        seg->next->next->prev = seg;
        seg->next = seg->next->next;
        seg->segment_size += seg->next->segment_size;
    }
}

/**
 * Implementation for manage page table list 
 */

void page_list_init(page_list_t *list){
    list->head = NULL;
    list->tail = NULL;
    list->size = 0;
}

void page_list_append(page_list_t *list, page_t *new_page){
    list->tail->next = new_page;            // Last element of the list points to new_page
    new_page->prev = list->tail;            // The prev pointer of the new_page points to list tail
    list->tail = new_page;                  // Change the list tail with new_page
    new_page->next = NULL;                  // Next element of new_page is NULL, so it is surely the last page
    list->size += 1;                        // Increment the size of the allocated pages
}

void page_list_push(page_list_t *list, page_t *new_page){
    new_page->next = list->head;            // The next page of new_page is the head of the list
    new_page->prev = NULL;                  // It is surely the first page, no pointer to previous page
    list->head = new_page;                  // Also commits the head of the list to the (first) new_page
    list->size += 1;                        // Increment the size of the allocated pages
}

page_t *head(page_list_t *list){
    return list->head;
}

page_t *page_list_pop(page_list_t *list){
    page_t *res = list->head;               // Save the page that will be return
    list->head = list->head->next;          // List head point to next page cause the first will be popped
    list->head->prev = NULL;                // List (new) head previous is set to NULL cause there are no page before it
    list->size -= 1;                        // Decreasing the size of the page list
    return res; 
}

uint32_t page_list_size(page_list_t *list){
    return list->size;
}

page_t *page_next(page_t *page){
    return page->next;
}