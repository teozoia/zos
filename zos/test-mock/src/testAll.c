#include "unity.h"

extern void test_mem_free_page(void);
extern void test_mem_alloc_page(void);


void setUp(){
	TEST_MESSAGE("Testing with mock");
}

void tearDown(){}

int testAll(void) {

    UNITY_BEGIN();

    RUN_TEST(test_mem_free_page);
    RUN_TEST(test_mem_alloc_page);

    return UNITY_END();
}