#include "unity.h"
#include "Mockmem.h"

void test_mem_free_page(void){

	unsigned char retval = 2;

	void *ptrExpectedFeatures = 0x12345;

  	free_page_Expect(ptrExpectedFeatures);

	TEST_ASSERT_EQUAL(1, retval);
}


void test_mem_alloc_page(void){

	void *fake_atags = 0x123;

	page_t fake_page;
	fake_page.vaddr = 0x555;
	fake_page.next = 0;
	fake_page.flags.kernel_page = 1;
    fake_page.flags.allocated = 1;

	mem_init_Expect(fake_atags);
	alloc_page_ExpectAndReturn(&fake_page);

	page_t *retval = alloc_page();

	TEST_ASSERT_EQUAL_HEX32(0x555, retval->vaddr);
	TEST_ASSERT_BITS(1, 1, retval->flags.kernel_page);
}