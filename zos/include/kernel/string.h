#ifndef _STRING_
#define _STRING_

#include <stddef.h>
#include <stdint.h>

void *memcpy(void *destination, const void *source, size_t num);
void *memmove(void *destination, const void *source, size_t num);
void *memset(void *ptr, int value, size_t num);
size_t strlen(const char *str);

void bzero(void *dest, int bytes);

#endif