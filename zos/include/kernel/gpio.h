#ifndef _GPIO_
#define _GPIO_

#include <stdbool.h>												// Needed for bool and true/false
#include <stdint.h>													// Needed for uint8_t, uint32_t, etc

#define MMIO_BASE       0x3F000000

#define GPFSEL0         (MMIO_BASE + 0x00200000)
#define GPFSEL1         (MMIO_BASE + 0x00200004)
#define GPFSEL2         (MMIO_BASE + 0x00200008)
#define GPFSEL3         (MMIO_BASE + 0x0020000C)
#define GPFSEL4         (MMIO_BASE + 0x00200010)
#define GPFSEL5         (MMIO_BASE + 0x00200014)
#define GPSET0          (MMIO_BASE + 0x0020001C)
#define GPSET1          (MMIO_BASE + 0x00200020)
#define GPCLR0          (MMIO_BASE + 0x00200028)
#define GPLEV0          (MMIO_BASE + 0x00200034)
#define GPLEV1          (MMIO_BASE + 0x00200038)
#define GPEDS0          (MMIO_BASE + 0x00200040)
#define GPEDS1          (MMIO_BASE + 0x00200044)
#define GPHEN0          (MMIO_BASE + 0x00200064)
#define GPHEN1          (MMIO_BASE + 0x00200068)
#define GPPUD           (MMIO_BASE + 0x00200094)
#define GPPUDCLK0       (MMIO_BASE + 0x00200098)
#define GPPUDCLK1       (MMIO_BASE + 0x0020009C)


typedef enum {
	GPIO_INPUT = 0b000,												// 0
	GPIO_OUTPUT = 0b001,											// 1
	GPIO_ALTFUNC5 = 0b010,											// 2
	GPIO_ALTFUNC4 = 0b011,											// 3
	GPIO_ALTFUNC0 = 0b100,											// 4
	GPIO_ALTFUNC1 = 0b101,											// 5
	GPIO_ALTFUNC2 = 0b110,											// 6
	GPIO_ALTFUNC3 = 0b111,											// 7
} GPIOMODE;


bool gpio_setup (uint8_t gpio, GPIOMODE mode);

bool gpio_output (uint8_t gpio, bool on);

bool gpio_input (uint8_t gpio);

void mmio_write(uint32_t reg, uint32_t data);

uint32_t mmio_read(uint32_t reg);

void delay(int32_t count);


#endif

