#ifndef _ATAG_
#define _ATAG_

#include <stddef.h>
#include <stdint.h>
#include <atag.h>
#include <uart.h>

/* list of possible tags */
#define ATAG_NONE       0x00000000
#define ATAG_CORE       0x54410001
#define ATAG_MEM        0x54410002
#define ATAG_VIDEOTEXT  0x54410003
#define ATAG_RAMDISK    0x54410004
#define ATAG_INITRD2    0x54420005
#define ATAG_SERIAL     0x54410006
#define ATAG_REVISION   0x54410007
#define ATAG_VIDEOLFB   0x54410008
#define ATAG_CMDLINE    0x54410009

/* structures for each atag */
typedef struct atag_header {
    uint32_t size; /* length of tag in words including this header */
    uint32_t tag;  /* tag type */
} atag_header_t;

typedef struct atag_core {
    uint32_t flags;
    uint32_t pagesize;
    uint32_t rootdev;
} atag_core_t;

typedef struct atag_mem {
    uint32_t size;
    uint32_t start;
} atag_mem_t;

typedef struct atag_videotext {
    uint8_t x;
    uint8_t y;
    uint16_t video_page;
    uint8_t video_mode;
    uint8_t video_cols;
    uint16_t video_ega_bx;
    uint8_t video_lines;
    uint8_t video_isvga;
    uint16_t video_points;
} atag_videotext_t;

typedef struct atag_ramdisk {
    uint32_t flags;
    uint32_t size;
    uint32_t start;
} atag_ramdisk_t;

typedef struct atag_initrd2 {
    uint32_t start;
    uint32_t size;
}atag_initrd2_t;

typedef struct atag_serialnr {
    uint32_t low;
    uint32_t high;
} atag_serialnr_t;

typedef struct atag_revision {
    uint32_t rev;
} atag_revision_t;

typedef struct atag_videolfb {
    uint16_t lfb_width;
    uint16_t lfb_height;
    uint16_t lfb_depth;
    uint16_t lfb_linelength;
    uint16_t lfb_base;
    uint16_t lfb_size;
    uint8_t red_size;
    uint8_t red_pos;
    uint8_t green_size;
    uint8_t green_pos;
    uint8_t blue_size;
    uint8_t blue_pos;
    uint8_t rsvd_size;
    uint8_t rsvd_pos;
} atag_videolfb_t;

typedef struct atag_cmdline {
    char cmdline[1];
} atag_cmdline_t;

typedef struct atag {
    atag_header_t hdr;
    union {
        atag_core_t core;
        atag_mem_t mem;   
        atag_videotext_t videotext;
        atag_ramdisk_t ramdisk;
        atag_initrd2_t initrd2;
        atag_serialnr_t serialnr;
        atag_revision_t revision;
        atag_videolfb_t videolfb;
        atag_cmdline_t cmdline;
    } u;
} atag_t;

void dump_atags(atag_t *ptr);

uint32_t get_page_size(atag_t *tag);
uint32_t get_mem_start(atag_t *tag);
uint32_t get_mem_size(atag_t *tag);

#endif