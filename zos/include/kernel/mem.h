#ifndef _MEM_
#define _MEM_

#include <stdint.h>
#include <atag.h>

#define PAGE_SIZE 4096
#define KERNEL_HEAP_SIZE (1024*1024)

typedef struct page_flags {
    uint8_t allocated: 1;           // This page is allocated to something
    uint8_t kernel_page: 1;         // This page is a part of the kernel
    uint8_t kernel_heap_page: 1;    // This page is a part of the kernel
    uint32_t reserved: 29;
} page_flags_t;

typedef struct page {
    uint32_t vaddr;                 // The virtual address that maps to this page   
    page_flags_t flags;
    void *next;
    void *prev;
} page_t;

typedef struct page_list {
    page_t *head; 
    page_t *tail; 
    uint32_t size;
} page_list_t;

/**
 * impliment kmalloc as a linked list of allocated segments.
 * Segments should be 4 byte aligned.
 * Use best fit algorithm to find an allocation
 */
typedef struct heap_segment{
    struct heap_segment *next;
    struct heap_segment *prev;
    uint32_t is_allocated;
    uint32_t segment_size;  // Includes this header
} heap_segment_t;

void page_list_init(page_list_t *list);
void page_list_append(page_list_t *list, page_t *new_page);
void page_list_push(page_list_t *list, page_t *new_page);
page_t *head(page_list_t *list);
page_t *page_list_pop(page_list_t *list);
uint32_t page_list_size(page_list_t *list);

page_t *page_next(page_t *page);

void mem_init(atag_t *atags);

void *alloc_page(void);
void free_page(void *ptr);

/* Heap */

void heap_init(uint32_t heap_start);
void *kmalloc(uint32_t bytes);
void kfree(void *ptr);

#endif