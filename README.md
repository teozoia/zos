# ZOS
_Raspberry Pi 3b+ Operating System from scratch_

**Release** --> zos/build/zos.img

### Build it your own 
Download the docker image from offiacial DockerHub teozoia/raspi3-zos:latest, under /root/zos there is the gcc folder already compiled form Raspberry ARM processor and the qemu forlder. Change directory to /root/zos/zos/build and `$make build`.

### Qemu emulation
Supposing that you've already pulled teozoia/raspi3-zos:latest (from DockerHub):
* Place the project folder in /root/zos
* Change directory to /root/zos/zos/build
* Launch qemu command: `$qemu-system-aarch64 -m 1024 -M raspi3 -serial stdio -kernel zos.img`.

Main open project where I take some lines of code
* https://github.com/Rohansi/RaspberryPi/
* https://github.com/jsandler18/raspi-kernel/

_Special thanks to Azeria, Rohansi and jsandler18 for tutorials and inspiration to build an ARM bare-metal kernel from scratch._

---

Some links and reference
 * https://stackoverflow.com/questions/31787617/what-is-the-current-execution-mode-exception-level-etc
 * https://static.docs.arm.com/100878/0100/fundamentals_of_armv8_a_100878_0100_en.pdf
 * https://github.com/Rohansi/RaspberryPi/blob/master/source/atag.cpp
 * https://github.com/dtczhl/dtc-raspberry-bare-metal
 * https://www.raspberrypi.org/forums/viewtopic.php?t=10889
 * https://elinux.org/R-Pi_configuration_file
 * https://github.com/dwelch67/raspberrypi